//
//  ViewController.m
//  Happiness
//
//  Created by xcode on 21.10.15.
//  Copyright (c) 2015 VSU. All rights reserved.
//

#import "ViewController.h"
#import "FaceView.h"

@interface ViewController () <FaceViewDataSource>

//Iboutlet связь будет задана в story board
@property (nonatomic, strong) IBOutlet FaceView *faceView;

@end

@implementation ViewController

-(void)setHappiness:(int)happiness{
    _happiness = happiness;
    [self.faceView setNeedsDisplay];
}

- (void) setFaceView:(FaceView *)faceView {
    _faceView = faceView;
    self.faceView.dataSource = self;
    [self.faceView addGestureRecognizer:[[UIPanGestureRecognizer alloc]
                                         initWithTarget:self
                                         action:@selector(handleHappinessGesture:)]];
    [self.faceView addGestureRecognizer:[[UIPinchGestureRecognizer alloc]
                                         initWithTarget:self.faceView
                                         action:@selector(handlePinchGesture:)]];
}

- (float)happinessLevelForFaceView:(FaceView *)faceView {
    return (self.happiness - 50.0)/50.0;
}

- (void)handleHappinessGesture:(UIPanGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateChanged ||
        gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        CGPoint translation = [gestureRecognizer translationInView:self.faceView];
        self.happiness -= translation.y / 2;
        [gestureRecognizer setTranslation:CGPointZero inView:self.faceView];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
