//
//  main.m
//  Happiness
//
//  Created by xcode on 21.10.15.
//  Copyright (c) 2015 VSU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
