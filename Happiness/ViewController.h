//
//  ViewController.h
//  Happiness
//
//  Created by xcode on 21.10.15.
//  Copyright (c) 2015 VSU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic) int happiness; //0 - грустно, 100 - радостно

@end

