//
//  FaceView.h
//  Happiness
//
//  Created by xcode on 21.10.15.
//  Copyright (c) 2015 VSU. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FaceView;
@protocol FaceViewDataSource <NSObject>

- (float)happinessLevelForFaceView:(FaceView *)faceView;

@end

@interface FaceView : UIView

@property (nonatomic, weak) id <FaceViewDataSource> dataSource;
@property (nonatomic) CGFloat scale;

- (void)handlePinchGesture:(UIPinchGestureRecognizer *)gestureRecognizer;

@end
