//
//  FaceView.m
//  Happiness
//
//  Created by xcode on 21.10.15.
//  Copyright (c) 2015 VSU. All rights reserved.
//

#import "FaceView.h"

#define DEFAULT_SCALE 0.9

@implementation FaceView

@synthesize scale = _scale;

- (void)setup {
    self.contentMode = UIViewContentModeRedraw;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib {
    [self setup];
}

- (CGFloat)scale {
    if (_scale == 0) {
        return DEFAULT_SCALE;
    }
    return _scale;
}

- (void)setScale:(CGFloat)scale {
    if (scale != _scale) {
        _scale = scale;
    }
    [self setNeedsDisplay];
}

- (void)handlePinchGesture:(UIPinchGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateChanged ||
        gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        self.scale *= gestureRecognizer.scale;
        gestureRecognizer.scale = 1;
    }
}

//рис задан круг задан радиус
- (void)drawCircleAtPoint:(CGPoint)point withRadius:(CGFloat)radius inContext:(CGContextRef)context{
    UIGraphicsPushContext(context);
    //объявили контур который будем создавать
    CGContextBeginPath(context);
    CGContextAddArc(context, point.x, point.y, radius, 0, 2*M_PI, YES); //крайний по часовой или против, перед какой сектор рисовать в радианах
    CGContextStrokePath(context);
    [[UIColor blueColor] setFill];//заливка внутри
    CGContextFillPath(context);//заливка
    UIGraphicsPopContext();
    
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    //получаем контекст для рисования
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //Нарисовать обращение к bound центр лица
    CGPoint midPoint;
    midPoint.x = self.bounds.origin.x + self.bounds.size.width/2;
    midPoint.y = self.bounds.origin.y + self.bounds.size.height/2;
    
    CGFloat size = self.bounds.size.width/2;
    //Проверка какой режим портретный или ландшафный
    if (self.bounds.size.height < self.bounds.size.width) {
        size = self.bounds.size.height/2;
    }
    
    size *= self.scale;
    //первый контекст, фя задает ширину линий
    CGContextSetLineWidth(context, 5.0);
    //синий цвет
    [[UIColor blueColor] setStroke];
    [self drawCircleAtPoint:midPoint withRadius:size inContext:context];
    
    
#define EYE_H 0.35
#define EYE_V 0.35
#define EYE_RADIUS 0.1
    CGPoint eyePoint;
    eyePoint.x = midPoint.x - size * EYE_H;
    eyePoint.y = midPoint.y - size * EYE_V;
    [self drawCircleAtPoint:eyePoint withRadius:size * EYE_RADIUS inContext:context];
    
    eyePoint.x += size * EYE_H * 2;
    
    [self drawCircleAtPoint:eyePoint withRadius:size * EYE_RADIUS inContext:context];
    
    //Сохранилось временное состоние то что мы задали выше
    //UIGraphicsPushContext(context);
    //что то другое задали
    //UIgraphicsPopcontext обратно
    
    //Drawing smile
#define MOUTH_H 0.45
#define MOUTH_V 0.40
#define MOUTH_SMILE 0.25
    
    CGPoint mouthStart;
    mouthStart.x = midPoint.x - MOUTH_H * size;
    mouthStart.y = midPoint.y + MOUTH_V * size;
    
    CGPoint mouthEnd = mouthStart;
    mouthEnd.x += MOUTH_H * size * 2;
    
    CGPoint mouthCP1 = mouthStart;
    mouthCP1.x += MOUTH_H * size * 2/3;
    
    CGPoint mouthCP2 = mouthEnd;
    mouthCP2.x -= MOUTH_H * size * 2/3;
    
    float smile = [self.dataSource happinessLevelForFaceView:self];
    if (smile < -1) {
        smile = -1;
    }
    if (smile > 1) {
        smile = 1;
    }
    
    CGFloat smileOffset = MOUTH_SMILE * size * smile;
    
    mouthCP1.y += smileOffset;
    mouthCP2.y += smileOffset;
    
    CGContextBeginPath(context);
    CGContextMoveToPoint(context, mouthStart.x, mouthStart.y);
    CGContextAddCurveToPoint(context, mouthCP1.x, mouthCP1.y, mouthCP2.x, mouthCP2.y, mouthEnd.x, mouthEnd.y);
    
    CGContextStrokePath(context);
    
    
}


@end
