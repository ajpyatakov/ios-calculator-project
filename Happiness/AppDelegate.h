//
//  AppDelegate.h
//  Happiness
//
//  Created by xcode on 21.10.15.
//  Copyright (c) 2015 VSU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

