//
//  CalculatorBrain.h
//  Calculator
//
//  Created by Пятаков Александр Юрьевич on 16.09.15.
//  Copyright (c) 2015 VSU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CalculatorBrain : NSObject

- (void)pushOperand:(double)operand;
- (double)popOperand;
- (void) clearOperands;
- (double)performOperation:(NSString *)operation;


@end
