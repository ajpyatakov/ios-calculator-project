//
//  CalculatorViewController.m
//  Calculator
//
//  Created by Пятаков Александр Юрьевич on 09.09.15.
//  Copyright (c) 2015 VSU. All rights reserved.
//

#import "CalculatorViewController.h"
#import "CalculatorBrain.h"

@interface CalculatorViewController ()
@property (weak, nonatomic) IBOutlet UILabel *display;
@property (weak, nonatomic) IBOutlet UILabel *operationsLabel;
@property (nonatomic) BOOL userIsInTheMiddleOfEnteringNumber;
@property (nonatomic, strong) CalculatorBrain *brain;
@property (nonatomic, strong) NSMutableArray *visibleOperationsArray;

@end

@implementation CalculatorViewController

- (NSMutableArray *)visibleOperationsArray {
    if (!_visibleOperationsArray) {
        _visibleOperationsArray = [[NSMutableArray alloc] init];
    }
    return _visibleOperationsArray;
}

- (CalculatorBrain *)brain {
    if (!_brain) {
        _brain = [[CalculatorBrain alloc] init];
    }
    return _brain;
}

- (IBAction)digitPressed:(id)sender {
    NSString *digit = [sender currentTitle];
    if (self.userIsInTheMiddleOfEnteringNumber) {
        self.display.text = [self.display.text stringByAppendingString:digit];
    }
    else {
        self.display.text = digit;
        self.userIsInTheMiddleOfEnteringNumber = YES;
    }
}

- (IBAction)operationPressed:(UIButton *)sender {
    if (self.userIsInTheMiddleOfEnteringNumber) {
        [self enterPressed];
    }
    [self.visibleOperationsArray addObject:sender.currentTitle];
    [self appendToOperandsLabel:sender.currentTitle];
    double result = [self.brain performOperation:sender.currentTitle];
    if (result != result) {
        NSString *message = @"Invalid operation!";
        if ([@"/" isEqualToString:sender.currentTitle]) {
            message = @"Division by zero!";
        }
        if ([@"\u221A" isEqualToString:sender.currentTitle]) {
            message = @"Square root from negative number!";
        }
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                             message:message
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
        [errorAlert show];
    }
    NSString *resultString = [NSString stringWithFormat:@"%.10g", result];
    self.display.text = resultString;
}

- (IBAction)enterPressed {
    [self.brain pushOperand:[self.display.text doubleValue]];
    [self.visibleOperationsArray addObject:self.display.text];
    [self appendToOperandsLabel:self.display.text];
    self.userIsInTheMiddleOfEnteringNumber = NO;
}

- (IBAction)clearPressed {
    self.display.text = [NSString stringWithFormat:@"%d", 0];
}

- (IBAction)clearAllPressed {
    [self.brain clearOperands];
    self.operationsLabel.text = @"";
    [self.visibleOperationsArray removeAllObjects];
    self.display.text = [NSString stringWithFormat:@"%d", 0];
}

- (IBAction)delimiterPressed {
    if (![self.display.text containsString:@"."]) {
        self.display.text = [self.display.text stringByAppendingString:@"."];
    }
}

                                 
- (void)appendToOperandsLabel:(NSString *)stringToAppend {
    if ([self.visibleOperationsArray count] >= 6) {
        [self.visibleOperationsArray removeObjectAtIndex:0];
        self.operationsLabel.text = [self.visibleOperationsArray componentsJoinedByString:@" "];
        self.operationsLabel.text = [self.operationsLabel.text stringByAppendingString:@" "];
    } else {
        self.operationsLabel.text = [self.operationsLabel.text stringByAppendingString:stringToAppend];
        self.operationsLabel.text = [self.operationsLabel.text stringByAppendingString:@" "];
    }
}

@end
