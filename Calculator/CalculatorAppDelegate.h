//
//  CalculatorAppDelegate.h
//  Calculator
//
//  Created by Пятаков Александр Юрьевич on 09.09.15.
//  Copyright (c) 2015 VSU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalculatorAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
