//
//  CalculatorBrain.m
//  Calculator
//
//  Created by Пятаков Александр Юрьевич on 16.09.15.
//  Copyright (c) 2015 VSU. All rights reserved.
//

#import "CalculatorBrain.h"

@interface CalculatorBrain()

@property (nonatomic, strong) NSMutableArray *operandStack;

@end

@implementation CalculatorBrain

- (NSMutableArray *)operandStack {
    if (!_operandStack) {
        _operandStack = [[NSMutableArray alloc] init];
    }
    return _operandStack;
}

- (void)pushOperand:(double)operand {
//    [self.operandStack addObject:[NSNumber numberWithDouble:operand]];
    [self.operandStack addObject:@(operand)];
}

- (double)popOperand {
    NSNumber *operandObject = [self.operandStack lastObject];
    if (operandObject) {
        [self.operandStack removeLastObject];
    }
    return operandObject.doubleValue;
}

- (double)performOperation:(NSString *)operation {
    double result = 0;
    if ([operation isEqualToString:@"*"]) {
        result = [self popOperand] * [self popOperand];
    }
    else if ([@"+" isEqualToString:operation]) {
        result = [self popOperand] + [self popOperand];
    }
    else if ([@"-" isEqualToString:operation]) {
        result = [self popOperand] - [self popOperand];
    }
    else if ([@"/" isEqualToString:operation]) {
        double secondOperand = [self popOperand];
        double firstOperand = [self popOperand];
        result = firstOperand / secondOperand;
    }
    else if ([@"sin" isEqualToString:operation]) {
        result = sin([self popOperand]);
    }
    else if ([@"cos" isEqualToString:operation]) {
        result = sin([self popOperand]);
    }
    else if ([@"\u221A" isEqualToString:operation]) {
        result = sqrt([self popOperand]);
    }
    else if ([@"\u03C0" isEqualToString:operation]) {
        result = M_PI;
    }
    [self pushOperand:result];
    return result;
}

- (void)clearOperands {
    [self.operandStack removeAllObjects];
}


@end
